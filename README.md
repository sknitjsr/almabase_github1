
# Problem Statement :
> ***Find the n most popular repositories of a given organization on Github
(Eg:​ https://github.com/google​ ) based on the number of forks. For each such repo find the top m
committees and their commit counts.***
>
>
# Introduction :
> ***This repository contains the solution of Above Problem Statment as Android Application.***

# Instructions to Run this Application :
> ***Download APK from [here](https://drive.google.com/file/d/1oOZqzxJe6F1C84p3fiEy-YF_qm3qNrdD/view?usp=sharing).***  
>
> ***Install this APK in any android device or directly click [here](https://www.apkonline.net/runapk/create-androidapk.html?app=android_blank&apk=%2Fvar%2Fwww%2Fhtml%2Fweboffice%2Fmydata%2F1604760295%2FNewDocuments%2F%2FAlmaBaseGITHUB.apk) to run in online free android emulator.***  
>
> ***Enter Input Values and you will get your result.***  
>
# Solution Approach:
> ***For example : we need to find top N repositories and top M committes for each repo for google***   
>> ***There are total 1886 repos by google, we need to sort them according to fork counts.***  
>> ***There is no inbuilt sort parameter in github [API Call](https://api.github.com/orgs/google/repos?per_page=100&page=1) that can sort repositories according to fork counts.***
>> ***So, We need to store all 1886 repos and their fork count in array list by making API calls to github. Also per request github will give max 100 requests so for 1886 repos we need to make 19 API calls.***  
>> ***Now, we will sort this list and for top N repositories.***  
>> ***For each repository, we will make [API call](https://api.github.com/repos/google/tracing-framework/collaborators) to fetch contributions,(By default, this API Call give us data in sorted manner according to commit counts.***  
>> ***As we only need top M committees so we will fetch only top M. (And here too per API request we get data of 100 committees only).***  
>
# Directory Path for Quick References :
> ***For Java Backend Files***  :
>> ***app/src/main/java/com/almabase/almabase_github/HTTP_REQUEST.java***   
>> ***app/src/main/java/com/almabase/almabase_github/MainActivity.java***  
>> ***app/src/main/java/com/almabase/almabase_github/RepositoryForkCount.java***
>
> ***For LayOut Files :***  
>> ***app/src/main/res/layout/activity_main.xml***
>
> ***Android Manifest file :***
>> ***app/src/main/AndroidManifest.xml***
>
# Output & ScreenShot
>![Imag1](Output_ScreenShot/1.jpg)..![Imag1](Output_ScreenShot/2.jpg)..![Imag1](Output_ScreenShot/3.jpg)..![Imag1](Output_ScreenShot/4.jpg)..![Imag1](Output_ScreenShot/5.jpg)..![Imag1](Output_ScreenShot/6.jpg)..![Imag1](Output_ScreenShot/7.jpg)..![Imag1](Output_ScreenShot/8.jpg)..![Imag1](Output_ScreenShot/9.jpg)..![Imag1](Output_ScreenShot/10.jpg)..![Imag1](Output_ScreenShot/11.jpg)..![Imag1](Output_ScreenShot/12.jpg)..![Imag1](Output_ScreenShot/13.jpg)..![Imag1](Output_ScreenShot/14.jpg)
>
# Developed By
> ## Sahil Kaushik
> ## nitjsr.sk@gmail.com