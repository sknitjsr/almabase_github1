package com.almabase.almabase_github;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;


public class MainActivity extends AppCompatActivity {
    //Declaring components and global variables
    EditText organisationName;
    TextView responseView;
    ProgressBar progressBar;
    TextView statusView;
    Button queryButton;
    EditText valueofn;
    EditText valueofm;
    int NUMBER_OF_REPOSITORIES;
    int NUMBER_OF_COMMITTEES;
    String ORGANISATION_NAME;
    ArrayList<RepositoryForkCount> repo;
    final int MAX_PAGE_SIZE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Intializing All Components
        responseView = (TextView) findViewById(R.id.responseView);
        organisationName = (EditText) findViewById(R.id.organisationName);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        statusView = (TextView) findViewById(R.id.statusView);
        valueofn = (EditText) findViewById(R.id.valueofn);
        valueofm = (EditText) findViewById(R.id.valueofm);
        queryButton = (Button) findViewById(R.id.queryButton);
        repo = new ArrayList<RepositoryForkCount>();

        //setting onClickListener on search button
        queryButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                //storing values of n and m
                try {
                    String n = valueofn.getText().toString();
                    String m = valueofm.getText().toString();
                    NUMBER_OF_REPOSITORIES = Integer.parseInt(n);
                    NUMBER_OF_COMMITTEES = Integer.parseInt(m);
                    ORGANISATION_NAME = organisationName.getText().toString().trim();
                    responseView.setText("");
                    statusView.setText("");
                    repo.clear();
                    //check for internet connectivity
                    if (!isNetworkAvailable()) {
                        //display error message
                        showMessage(getString(R.string.internet_error_message));
                    } else {
                        //check for value of n
                        if (NUMBER_OF_REPOSITORIES == 0) {
                            //display invalid input message
                            showMessage(getString(R.string.invalid_norm_message));
                        } else {
                            //start a thread to make API requests
                            new GetRepoForOrganisation().execute();
                        }
                    }
                } catch (Exception e) {
                    Log.d("main" , e.getMessage());
                    showMessage("Enter value for m and n");

                }

            }
        });

    }


/*
Approach Used :
1. Fetch All repositories of a organisation.
2. Store repo names and number of forks in ArrayList
3. Sort them according to number of forks.
4. For top N repositories , fetch top M commiitees and commit count.
 */

    //Thread task to fetch repo of organisation
    private class GetRepoForOrganisation extends AsyncTask<Void, Void, Void> {
        boolean invalid_URL = false;
        @Override
        protected void onPreExecute() {
            setAllEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
            statusView.setText("Fetching top N Repositories...." + "\n" + "It may take time due to Github API rate limit.." + "\n");

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Making a request to url and getting response
            HTTP_REQUEST sh = new HTTP_REQUEST();
            //Fetching All repositories
            for (int i = 1; ; i++) {
                //create API URL
                String url = createAPIUrlForOrganisation(ORGANISATION_NAME, i, MAX_PAGE_SIZE);
                String jsonStr = sh.makeServiceCall(url);
                if (jsonStr == null) {
                    //for invalid URL
                    invalid_URL = true;
                    break;
                }
                Log.e("JSON", "Response from url: " + jsonStr);
                if (jsonStr != null) {
                    try {
                        //storing result in JSON Array.
                        JSONArray repository = new JSONArray(jsonStr);
                        if (repository.length() == 0)
                            break;
                        for (int j = 0; j < repository.length(); j++) {
                            JSONObject rep = repository.getJSONObject(j);
                            String repo_name = rep.getString("full_name");
                            String forks_name = rep.getString("forks");
                            //Adding repo name and fork count to array List
                            repo.add(new RepositoryForkCount(repo_name, Integer.parseInt(forks_name)));
                        }
                    } catch (final JSONException e) {
                        //show error message in logcat for debugging
                        Log.e("JSON", "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            //show error message to user
                            public void run() {
                                showMessage("JSON parsing error " + e.getMessage());
                            }
                        });

                    }

                } else {
                    //show error message in logcat for debugging
                    Log.e("JSON", "Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //show error message to user
                            showMessage("Couldn't get JSON from server");
                        }

                    });
                }
            }
            return null;
        }

        protected void onPostExecute(Void response) {
            super.onPostExecute(response);
            progressBar.setVisibility(View.GONE);
            setAllEnabled(true);

            //sort the repos
            Collections.sort(repo, (o1, o2) -> o2.getFork_count() - o1.getFork_count());
            statusView.setText("Fetched...." + "\n");

            if(invalid_URL) {
                responseView.setText("Enter a Valid Organisation Name");
                setAlltext("");
            }
            else
            //start fetching top m committes
            new GetTopCommittes().execute();
        }
    }

    //thread to fetch top m committes
    private class GetTopCommittes extends AsyncTask<Void, Void, Void> {
        //for storing final result
        String res = "";

        @Override
        protected void onPreExecute() {
            setAllEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
            statusView.setText("Fetching Top M Commiitees and their commit count..." + "\n");

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Making a request to url and getting response
            HTTP_REQUEST sh = new HTTP_REQUEST();

            // Making API request for top N repos
            for (int i = 0; i < NUMBER_OF_REPOSITORIES && i < repo.size(); i++) {
                res += ((i + 1) + ". " + repo.get(i).getRepo_name() + " " + repo.get(i).getFork_count() + "\n");
                //Since response of contributors is already in sorted order so find number of pages to fetch
                int numberOfPages = (NUMBER_OF_COMMITTEES / MAX_PAGE_SIZE) + 1;
                int count = 0;
                String repoName = repo.get(i).getRepo_name();
                for (int k = 1; k <= numberOfPages ; k++) {
                    if(count==NUMBER_OF_COMMITTEES)
                        break;
                    //create API URL

                    String url = createAPIUrlForContribution(repoName, k, MAX_PAGE_SIZE);
                    String jsonStr = sh.makeServiceCall(url);
                    if (jsonStr == null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showMessage("No data recieved! Possible due to Max API ratelimit. Check logcat");
                            }

                        });

                        break;
                    }
                    //for debugging purpouse
                    Log.e("JSON", "Response from url: " + jsonStr);
                    if (jsonStr != null) {
                        try {
                            //storing committees
                            JSONArray committees = new JSONArray(jsonStr);

                            if (committees.length() == 0)
                                break;
                            for (int j = 0; j < committees.length() ; j++) {
                                count++;
                                JSONObject c = committees.getJSONObject(j);
                                String repo_name = c.getString("login");
                                String forks_name = c.getString("contributions");
                                res += ("    " + (count) + ". " + repo_name + " " + forks_name + "\n");

                                if(count==NUMBER_OF_COMMITTEES)
                                    break;
                            }
                        } catch (final JSONException e) {
                            //message in debug if error occurs
                            Log.e("JSON", "Json parsing error: " + e.getMessage());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showMessage("Error" + e.getMessage());
                                }
                            });

                        }

                    } else {
                        Log.e("JSON", "Couldn't get json from server.");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showMessage("Couldn't get json from server. Check LogCat for possible errors!");
                            }

                        });
                    }
                }
                res += "\n\n";
            }

            return null;
        }

        protected void onPostExecute(Void response) {
            super.onPostExecute(response);
            setAllEnabled(true);
            progressBar.setVisibility(View.GONE);
            statusView.setText("Top " + NUMBER_OF_REPOSITORIES + " repository of Organisation : " + ORGANISATION_NAME + "\nAnd their top "  + NUMBER_OF_COMMITTEES + " committes are :\n\n" );
            setAlltext("");
            responseView.setText(res);

        }

    }

    //method to create api url to fetch repos
    private String createAPIUrlForOrganisation(String organisationName, int pageNumber, int pageSize) {
        return "https://api.github.com/orgs/" + organisationName + "/repos" + "?per_page=" + pageSize + "&page=" + pageNumber;
    }

    //method to create api url for top m committees
    protected String createAPIUrlForContribution(String repositoryname, int pageNumber, int pageSize) {
        return "https://api.github.com/repos/" + repositoryname + "/contributors?per_page=" + pageSize + "&page=" + pageNumber;
    }

    //method to check for network permissions
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //method to display message using toast to user
    private void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
    //to enable and disable all edit texts and buttons
    private  void setAllEnabled(boolean value)
    {
        organisationName.setEnabled(value);
        valueofn.setEnabled(value);
        valueofm.setEnabled(value);
        queryButton.setEnabled(value);
    }

    private  void setAlltext(String val)
    {
        organisationName.setText(val);
        valueofm.setText(val);
        valueofn.setText(val);
    }

}
