package com.almabase.almabase_github;

//custom class to store repo name and fork count
public class RepositoryForkCount {
    public String repo_name;
    public int fork_count;

    RepositoryForkCount(String name , int count)
    {
        this.repo_name = name;
        this.fork_count = count;
    }

    public int getFork_count() {
        return fork_count;
    }
    public  String getRepo_name()
    {
        return  repo_name;
    }
    public String toString() {
        return repo_name + " " + fork_count + "\n";
    }

}
